FROM alpine:3.21@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c

COPY files/ /

RUN apk add --no-cache \
      msmtp=~1 \
      bash=~5 \
      run-parts~=4 \
      moreutils~=0

ENV TESTER_CROND_EXPRESSION="" \
    TESTER_CROND_LOGLEVEL="6"

ENTRYPOINT ["/usr/local/sbin/docker-entrypoint"]
CMD ["/usr/local/sbin/crond"]
